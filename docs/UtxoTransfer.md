# UtxoTransfer

Transfer of currency in the UTXO model

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inputs** | [**[Utxo]**](Utxo.md) |  | 
**outputs** | [**[Utxo]**](Utxo.md) |  | 
**currency** | [**Currency**](Currency.md) |  | 
**unspent** | **str** | Integer string in smallest unit (Satoshis) | 
**any string name** | **bool, date, datetime, dict, float, int, list, str, none_type** | any string name can be used but the value must be the correct type | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


